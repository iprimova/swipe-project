var fs = require('fs');
var CodeGen = require('swagger-js-codegen').CodeGen;

var file = 'api/swagger/swagger.json';
var swagger = JSON.parse(fs.readFileSync(file, 'UTF-8'));
//var nodejsSourceCode = CodeGen.getNodeCode({ className: 'Test', swagger: swagger});
//var angularjsSourceCode = CodeGen.getAngularCode({ className: 'ConcertService', swagger: swagger});
var tsSourceCode = CodeGen.getTypescriptCode({ className: 'ConcertService', swagger: swagger, imports: ['../../tyings/tsd.d.ts'] });
//console.log(nodejsSourceCode);
//console.log(angularjsSourceCode);
console.log(tsSourceCode);