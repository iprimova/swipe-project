'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');

var dummyjson = require('dummy-json');
var jstoxml = require('jstoxml');
var o2x = require('object-to-xml');



var imageSchema = '{\
  "likes" : {{int 0 4999}},\
  "description" : "{{lorem 5}}",\
  "name" : "{{theme}} Design item made by {{firstName}} {{lastName}} Agency from {{city}}",\
  "image" : "img{{@guid}}.png"\
}';

var imagesSchema = '[\
    {{#repeat 10}}\
    '+ imageSchema + '\
    {{/repeat}}\
  ]';
/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */

/**
 * Gets a certain image
 * Returns a single image for its ID
 * 
 * eventId String The image's id
 * currency String Preferred currency to use for price conversion, use [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217http://en.wikipedia.org/wiki/ISO_4217) as currency code
 * returns image
**/

/**
* Provide list of images, filtered out by theme or nationwide. The list supports paging.
*
* theme String The name of the theme to filter out images (optional)
* pageSize BiggDecimal Number of images returned (optional)
* pageNumber BigDecimal Page number (optional)
* currency String Preferred currency to use for price conversion, use [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217http://en.wikipedia.org/wiki/ISO_4217) as currency code
* returns List
**/
module.exports.getimages = function getimages(req, res, next) {

  var title = req.query.title || 'lacoste';
  var date = req.query.date || 10;
  var likes = req.query.likes || 1;
  var location = req.query.location || 'USA';



  var images = {};
  images['application/json'] = /*[{
    "likes" : 0.8008282,
    "description" : "aeiou",
    "name" : "aeiou",
    "image" : "aeiou"
  } ];*/
  JSON.parse(dummyjson.parse(imagesSchema, {mockdata: {"title" : theme, "correncyCode": currency}}));

  if (Object.keys(images).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.json(images[Object.keys(images)[0]]);
    //res.end(JSON.stringify(images[Object.keys(images)[0]] || {}, null, 2));
    } else {
      res.end();
    }
};
